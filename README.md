# Headless WebdriverIO setup

## Server Setup

This setup is based on a clean DigitalOcean droplet with Ubuntu 18.04 x64

#### Install and update essentials
```bash
apt update -y
apt install -y curl wget git unzip libnss3-dev
```

#### Install Node and NPM with NVM
```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
source ~/.bashrc
nvm install --lts
```

#### Install Google Chrome
```bash
curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
apt update -y
apt install -y google-chrome-stable
```

## Project Setup

#### Clone this project
```bash
git clone https://chris-bogaards@bitbucket.org/chris-bogaards/wdio-headless-demo.git
cd wdio-headless-demo/
npm install
```

#### Run the tests
```bash
./node_modules/.bin/wdio wdio.conf.js
```
